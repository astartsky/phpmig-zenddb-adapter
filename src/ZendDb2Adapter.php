<?php
namespace Astartsky\Phpmig;

use Phpmig\Adapter\AdapterInterface;
use Phpmig\Migration\Migration;
use \Zend\Db\Adapter\Adapter;
use Zend\Db\Adapter\Exception\ExceptionInterface;

class ZendDb2Adapter implements AdapterInterface
{
    /**
     * @var string
     */
    protected $tableName;

    /**
     * @var Adapter
     */
    protected $adapter;

    /**
     *
     *
     * @param Adapter $adapter
     * @param string $tableName
     */
    public function __construct(Adapter $adapter, $tableName)
    {
        $this->adapter = $adapter;
        $this->tableName = $tableName;
    }

    /**
     * Get all migrated version numbers
     *
     * @return array
     */
    public function fetchAll()
    {
        $statement = $this->adapter->createStatement("SELECT `version` FROM `{$this->tableName}`");
        $all = $statement->execute();

        $versions = array();
        foreach ($all as $result) {
            $versions[] = $result['version'];
        }

        return $versions;
    }

    /**
     * Up
     *
     * @param Migration $migration
     * @return $this
     */
    public function up(Migration $migration)
    {
        $statement = $this->adapter->createStatement("INSERT INTO `{$this->tableName}` (`version`) VALUES ({$migration->getVersion()})");
        $statement->execute();

        return $this;
    }

    /**
     * Down
     *
     * @param Migration $migration
     * @return $this
     */
    public function down(Migration $migration)
    {
        $statement = $this->adapter->createStatement("DELETE FROM `{$this->tableName}` WHERE `version` = :version");
        $statement->execute(array('version' => $migration->getVersion()));

        return $this;
    }

    /**
     * Is the schema ready?
     *
     * @return bool
     */
    public function hasSchema()
    {
        try {
            $statement = $this->adapter->createStatement("DESCRIBE `{$this->tableName}`");
            $result = $statement->execute();
            $schema = $result->current();
        } catch (ExceptionInterface $exception) {
            return false;
        }

        if (is_array($schema) && !empty($schema)) {
            return true;
        }

        return false;
    }

    /**
     * Create Schema
     *
     * @return $this
     */
    public function createSchema()
    {
        $statement = $this->adapter->createStatement("CREATE TABLE `Migration` (`version` VARCHAR(255) NOT NULL) ENGINE=InnoDB;");
        $statement->execute();

        return $this;
    }
} 
